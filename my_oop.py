# 1. Write a custom decorator

"""Write a python library that implements a decorator. The decorator should log a
line to a file before and after each execution of our decorated functions. The
log line should contain the name of the decorated function (i.e. "Running
function X"/"Done running function X"). The decorator should keep the name and
docstring of the original functions. The decorator should take the name of the
logfile as an argument, example:"""

from functools import wraps

def running_function(logged_file):

    def chopped_log(function):

        @wraps(function)
        def chopped_function(*args, **kwargs):
            f = open(logged_file, 'a')
            f.write(f"Entering my function: {function.__name__}\n")

            log_value = function(*args, **kwargs)

            f.write(f"Finished with my function: {function.__name__}\n")
            f.close()

            return log_value

        return chopped_function

    return chopped_log


@running_function("test_log.txt")
def first(some_text="", maybe_int=2):
    print(f"This is some {some_text}, maybe this is {maybe_int} nr.")
    return some_text
